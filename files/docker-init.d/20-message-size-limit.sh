#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_MESSAGE_SIZE_LIMIT" ]]; then
  exit 0
fi

update-postfix-config.sh <(echo "message_size_limit = $POSTFIX_MESSAGE_SIZE_LIMIT")
