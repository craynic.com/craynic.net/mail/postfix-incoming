#!/usr/bin/env bash

set -Eeuo pipefail

if [[ "$POSTFIX_HAPROXY" != "true" ]]; then
  exit 0
fi

update-postfix-config.sh <(echo "postscreen_upstream_proxy_protocol = haproxy")
