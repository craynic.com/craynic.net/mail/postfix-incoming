#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_POSTSRS_ADDRESS" ]]; then
  exit 0
fi

update-postfix-config.sh <(echo "recipient_canonical_maps = socketmap:inet:$POSTFIX_POSTSRS_ADDRESS:reverse")
