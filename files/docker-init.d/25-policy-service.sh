#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_POLICY_SERVICE_ADDRESS" ]]; then
  exit 0
fi

if ! [[ "$POSTFIX_POLICY_SERVICE_ADDRESS" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The policy_service address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the address. Are you sure you want that?"
fi

PATTERN="^(.*,[[:space:]]*)?(permit)([[:space:]]*($|,.*$))"
if [[ "$(postconf -h smtpd_recipient_restrictions)" =~ $PATTERN ]]; then
  RECIPIENT_RESTRICTIONS="${BASH_REMATCH[1]}check_policy_service inet:$POSTFIX_POLICY_SERVICE_ADDRESS, \
${BASH_REMATCH[2]}${BASH_REMATCH[3]}"

  update-postfix-config.sh <(echo "smtpd_recipient_restrictions = $RECIPIENT_RESTRICTIONS")
fi
