#!/usr/bin/env bash

set -Eeuo pipefail

postconf -Fe "smtp/inet/command=postscreen"
postconf -Fe "smtp/inet/process_limit=1"
postconf -M "smtpd/pass=smtpd pass - - n - - smtpd"
postconf -M "tlsproxy/unix=tlsproxy unix - - n - 0 tlsproxy"
postconf -M "dnsblog/unix=dnsblog unix - - n - 0 dnsblog"
