#!/usr/bin/env bash

set -Eeuo pipefail

cp -a /etc/postfix/main.cf /etc/postfix/main.cf.bak
