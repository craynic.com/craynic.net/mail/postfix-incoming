#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_RELAY_TRANSPORT" && -z "$POSTFIX_AMAVIS_ADDRESS" ]]; then
  echo "Missing either nexthop address for relay_transport or Amavis address to deliver e-mails." >/dev/stderr
  exit 1
fi

if [[ -n "$POSTFIX_RELAY_TRANSPORT" && -n "$POSTFIX_AMAVIS_ADDRESS" ]]; then
  echo "When Amavis address is set, relay_transport settings has no effect."
fi
