#!/usr/bin/env bash

set -Eeuo pipefail

SSL_DIR="/opt/postfix/ssl/"

inotifywait-dir.sh "$SSL_DIR" | while read -r line; do
  echo "$line"

  # re-apply SSL certificates
  /usr/local/sbin/update-ssl-certs-config.sh

  # reload postfix
  /usr/local/sbin/reload-postfix.sh
done
