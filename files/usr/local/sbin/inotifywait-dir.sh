#!/usr/bin/env bash

set -Eeuo pipefail

DIR="$1"
LAST_UPDATE=0
TIME_FORMAT="%s"

inotifywait -mr -e create -e move -e delete -e modify --format '%T' --timefmt "$TIME_FORMAT" "$DIR" \
  | while read -r TIMESTAMP; do
    if [[ "$TIMESTAMP" -ge "$LAST_UPDATE" ]]; then
      sleep 2

      LAST_UPDATE=$(date +"$TIME_FORMAT")

      echo "$(date -Iseconds) Changes detected in directory $DIR..."
    fi
  done
