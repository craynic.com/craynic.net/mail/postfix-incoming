#!/usr/bin/env bash

set -Eeuo pipefail

CERT_DIR="/opt/postfix/ssl"

if compgen -G "$CERT_DIR/*.crt" >/dev/null; then
  CERT_FILES=( "$CERT_DIR"/*.crt )
elif compgen -G "$CERT_DIR/*.pem" >/dev/null; then
  CERT_FILES=( "$CERT_DIR"/*.pem )
else
  echo "No SSL certificate file found, TLS can't be configured" >/dev/stderr
  exit 1
fi

CERT_FILE="${CERT_FILES[0]}"
update-postfix-config.sh <(echo "smtpd_tls_cert_file = $CERT_FILE"; echo "smtpd_tls_key_file = \$smtpd_tls_cert_file")

KEY_FILE="${CERT_FILE%.*}.key"
if compgen -G "$KEY_FILE" >/dev/null; then
  update-postfix-config.sh <(echo "smtpd_tls_key_file = $KEY_FILE")
fi
